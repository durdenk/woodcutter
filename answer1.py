products = {'a', 'b', 'c', 'd'}
machine_capacity = 8


def genCap(capacity=machine_capacity, used=0):
    if used == len(products) - 1:
        yield capacity, None
    else:
        for i in range(1, 2 + capacity - len(products) + used):
            yield i, genCap(capacity - i, used + 1)


def printCaps(caps, current=[]):
    if caps is None:
        print(dict(zip(products, current)))
        return
    for i in caps:
        printCaps(i[1], current + [i[0]])

for c in genCap():
    for i in c:
        print i
print set(genCap())
#printCaps(genCap())
