import copy
import time
from collections import Counter,defaultdict

order = {'a': 20000, 'b': 20000, 'c': 20000, 'd': 20000, 'e': 20000}
# stuff = ['a', 'b', 'c','d','e','f','g','h','i','j']
stuff = [i for i, v in order.items()]
max_size = 8
order_len = len(order)

x = ['x','a','z']
y = ['a','x','z']
print set(x)
print set(y)
if set(x) == set(y):
    print 'set>'

def calc_total_production(total_production, new_production):
    exception_keys_ = []

    for k, v in new_production.items():
        if k in order:  # We added other keys
            if k in total_production:
                total_production[k] += v
            else:
                total_production[k] = v

    for k, v in order.items():
        if k in total_production:
            if total_production[k] >= v:
                exception_keys_.append(k)

    if len(exception_keys_) == order_len:
        done_status = True
    else:
        done_status = False
    return done_status, exception_keys_, total_production


order_counter = Counter(order)

def calc_total_production2(total_production, new_production):
    order_counter_ = order_counter
    exception_keys_ = []
    total_production = Counter(total_production) + Counter(new_production)
    order_counter_.subtract(Counter(total_production))
    for k,v in order_counter_.items():
        if v <= 0:
            exception_keys_.append(k)

    if len(exception_keys_) == order_len:
        done_status = True
    else:
        done_status = False
    return done_status, exception_keys_, total_production


combinations_start_time1 = time.time()
for i in range(100000):
    new_production = defaultdict(float)
    new_production = {'b': 20000, 'c': 20000, 'd': 20000, 'e': 20000}
    exception_dict1 = {'a': 20000,}
    done_status1, exception_keys1, new_production1 = calc_total_production2(copy.deepcopy(new_production),
                                                                           exception_dict1)
print("--- %s seconds v2 ---" % (time.time() - combinations_start_time1))


combinations_start_time1 = time.time()
for i in range(100000):
    new_production = {'b': 20000, 'c': 20000, 'd': 20000, 'e': 20000}
    exception_dict1 = {'a': 20000,}
    done_status1, exception_keys1, new_production1 = calc_total_production(copy.deepcopy(new_production),
                                                                           exception_dict1)
print("--- %s seconds ---" % (time.time() - combinations_start_time1))


