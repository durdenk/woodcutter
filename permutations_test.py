import copy
import json
import math
import time
import functools
import line_profiler

from itertools import combinations

range1 = lambda start, end: range(start, end + 1)


def combinations_func(products, machine_capacity, current=[]):
    if len(current) == len(products):
        yield current
    else:
        for i in range1(1, machine_capacity):
            if sum(current + [i]) <= machine_capacity:
                for bar in combinations_func(products, machine_capacity, current=current + [i]):
                    yield bar


def get_combinations_of_subset(subset, machine_capacity):
    returnval = []
    for x in combinations_func(subset, machine_capacity):
        if sum(x) == machine_capacity:
            dict_ = dict(zip(subset, x))
            returnval.append(dict_)
    return returnval


order = {'a': 20000, 'b': 20000, 'c': 20000, 'd': 20000, 'e': 20000, 'f': 20000}
order = {'36': 650, '38': 2036, '40': 2636, '42': 3086, '44': 2636, '46': 392, '48': 392, '50': 392}
order_keys = [i for i, v in order.items()]
max_size = 18
order_len = len(order)
all_combinations = []

start_time = time.time()


def compare_with_order(compare_dict):
    subsracted_order = copy.deepcopy(order)
    for order_key in order_keys:
        if order_key in compare_dict:
            subsracted_order[order_key] -= compare_dict[order_key]
    return subsracted_order


def percentile(number1, number2):
    return (number1 / number2) * 100


def get_remaining_percent(items_comb):
    max_percent = 0
    max_scrap = 0
    substracted_ = compare_with_order(items_comb)
    for k, v in substracted_.items():
        if v < 0:
            value_percent = percentile(abs(v), order[k])
            if value_percent > max_percent:
                max_percent = value_percent
            if abs(v) > max_scrap:
                max_scrap = abs(v)
    return max_scrap, max_percent


def is_demand_satisfied(total_prod_):
    max_scrap, rem_percent = get_remaining_percent(total_prod_)
    if rem_percent <= 5 and max_scrap < 1000:
        return True
    return False


def stop_iterating(items_comb):
    max_scrap, rem_percent = get_remaining_percent(items_comb)
    if rem_percent >= 10 and max_scrap > 1000:
        # print 'stop_iterating>',rem_percent,max_scrap
        return True
    return False


counter = 0
for L in reversed(range(0, len(order_keys) + 1)):
    if L <= max_size:
        for subset in combinations(order_keys, L):
            subset_combinations = get_combinations_of_subset(subset, max_size)
            for comb_ in subset_combinations:
                for s in order_keys:
                    if s in comb_:
                        do_add_to = True
                        multiply_by = comb_[s]
                        order_quantity = order[s]
                        multiplier_dec = order_quantity / float(multiply_by)
                        multiplier = math.ceil(multiplier_dec)
                        items_comb = {}
                        for k, v in comb_.items():
                            items_comb[k] = v * multiplier
                        items_comb['combination'] = comb_
                        items_comb['multiplier'] = multiplier

                if not stop_iterating(items_comb):
                    counter += 1
                    items_comb['index'] = counter
                    all_combinations.append(items_comb)

print 'Combination Count', len(all_combinations)
print("--- %s Combination Count seconds ---" % (time.time() - start_time))

# Exception List
start_time = time.time()

exception_list = []
for i in range1(1, len(order.keys()) - 1):
    for combo in combinations(order.keys(), i):
        exception_dictionary = {'key': combo, 'value': []}
        for sub_comb in all_combinations:
            exists = False

            for combo_key in combo:
                if combo_key in sub_comb:
                    exists = True

            if not exists:
                exception_dictionary['value'].append(sub_comb)
        exception_list.append(exception_dictionary)

print("--- %s exception_list seconds ---" % (time.time() - start_time))


def get_exceptional_keys(prod_):
    exception_keys_ = []
    for k, v in order.items():
        if k in prod_:
            if prod_[k] >= v:
                exception_keys_.append(k)
    return tuple(exception_keys_)


def memoize(func):
    cache = func.cache = {}

    @functools.wraps(func)
    def memoized_func(*args, **kwargs):
        key_str = ''
        key = args[0]
        key.sort()
        for k in key:
            key_str += str(k)
        if key_str not in cache:
            cache[key_str] = func(*args, **kwargs)
        return cache[key_str]
    return memoized_func


@memoize
def calc_total_production(new_index, total_production, new_production):
    for k, v in new_production.items():
        if k in order_keys:  # We added other keys
            if k in total_production:
                total_production[k] += v
            else:
                total_production[k] = v
    exception_keys_ = get_exceptional_keys(total_production)
    if len(exception_keys_) == order_len:
        done_status = True
    else:
        done_status = False

    return done_status, exception_keys_, total_production


def find_next_best(best_index,level, possible_ways_, exception_keys, total_production, possible_way):
    possible_way = possible_way[:]
    for exception_list_ in exception_list:
        if set(exception_list_['key']) == set(exception_keys):
            for exc_num, new_production_set in enumerate(exception_list_['value'][:]):
                new_possible_way = list(possible_way)
                new_possible_way.append(copy.deepcopy(new_production_set))
                new_index = best_index + [new_production_set['index'], ]
                done_status, new_exception_keys, new_production = calc_total_production(new_index,
                                                                                        total_production.copy(),
                                                                                        new_production_set.copy())
                if level > 5:
                    return True,[]
                if stop_iterating(new_production):
                    return True,[]
                if done_status:
                    possible_ways_.append(new_possible_way)
                    if is_demand_satisfied(new_production):
                        return False, possible_ways_
                        print 'is_demand_satisfied>', new_production
                else:
                    level += 1
                    sub_index = new_index[:]
                    result,new_possible_ways = find_next_best(sub_index,level, possible_ways_, new_exception_keys[:],
                                                       new_production.copy(),
                                                       new_possible_way[:])
                    if result:
                        return result,new_possible_ways

    return True,possible_ways_

def predict_possible_ways():
    # Possible Ways
    possible_ways = []
    counter = 0

    for combo in all_combinations:
        counter += 1
        possible_ways_inner = []
        print 'combo>', counter, combo
        combination_start_time = time.time()
        possible_way = []
        possible_way.append(combo)
        exceptional_keys = get_exceptional_keys(combo)
        start_index = [combo['index'],]
        result,new_possible_ways = find_next_best(start_index,1, list(possible_ways_inner[:])
                                           , exceptional_keys, copy.deepcopy(combo),
                                           possible_way[:])
        if new_possible_ways:
            possible_ways.extend(new_possible_ways)
        if not result:
            pass
            #return possible_ways
        # print("--- %s possible_ways of combo %s seconds ---" % (time.time() - combination_start_time, str(json.dumps(combo))))

    return possible_ways

combinations_start_time = time.time()
possible_ways = predict_possible_ways()
print("--- %s possible_ways seconds ---" % (time.time() - combinations_start_time))
print 'possible_ways - len >',len(possible_ways)

# Possible Ways Sorting
start_time = time.time()

empty_order = {}
for k, v in order.items():
    empty_order[k] = 0

for way_ in possible_ways:
    len_ = len(way_)
    empty_order_rec = copy.deepcopy(empty_order)
    for w in way_:
        for k, v in w.items():
            if k in order:
                empty_order_rec[k] += v

    scrap_point = 0
    substracted_ = compare_with_order(empty_order_rec)
    for k, v in substracted_.items():
        if v < 0:
            scrap_point += abs(v)
    setup_point = (len_ - 1) * 50000 + scrap_point

    substracted_['scrap_point'] = scrap_point
    substracted_['setup_count'] = len_

    scrap_point += (len_ - 1) * 100

    way_.append(substracted_)
    way_.insert(0, setup_point)
    way_.insert(0, scrap_point)

possible_ways.sort(key=lambda x: x[0])

for w_num, way in enumerate(possible_ways):
    print 'Better Scrap Value>', w_num
    for w in way:
        print w

possible_ways.sort(key=lambda x: x[1])
for w_num, way in enumerate(possible_ways):
    print 'Better Setup>', w_num
    for w in way:
        print w
print("--- %s possible_ways sorting seconds ---" % (time.time() - start_time))
